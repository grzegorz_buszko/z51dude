/*
 * This Saleae Analyzer is based on the Saleae I2C Analyzer Code which is 
 * available from <https://support.saleae.com/saleae-api-and-sdk/protocol-analyzer-sdk>.
 * The modifications for capturing, decoding and analyzing the ABOV OCD protocol have
 * been made by Thomas Ries <tries@gmx.net>.
 */

#ifndef SERIAL_ANALYZER_H
#define SERIAL_ANALYZER_H

#include <Analyzer.h>
#include "ABOVAnalyzerResults.h"
#include "ABOVSimulationDataGenerator.h"

class ABOVAnalyzerSettings;
class ANALYZER_EXPORT ABOVAnalyzer : public Analyzer2
{
public:
	ABOVAnalyzer();
	virtual ~ABOVAnalyzer();
	virtual void SetupResults();
	virtual void WorkerThread();

	virtual U32 GenerateSimulationData( U64 newest_sample_requested, U32 sample_rate, SimulationChannelDescriptor** simulation_channels );
	virtual U32 GetMinimumSampleRateHz();

	virtual const char* GetAnalyzerName() const;
	virtual bool NeedsRerun();


#pragma warning( push )
#pragma warning( disable : 4251 ) //warning C4251: 'SerialAnalyzer::<...>' : class <...> needs to have dll-interface to be used by clients of class

protected: //functions
	void AdvanceToStartBit();
	void GetByte();
	bool GetBit( BitState& bit_state, U64& sck_rising_edge );
	bool GetBitPartOne( BitState& bit_state, U64& sck_rising_edge, U64& frame_end_sample );
	bool GetBitPartTwo();
	void RecordStartStopBit();

protected: //vars
	std::auto_ptr< ABOVAnalyzerSettings > mSettings;
	std::auto_ptr< ABOVAnalyzerResults > mResults;
	AnalyzerChannelData* mSda;
	AnalyzerChannelData* mScl;

	ABOVSimulationDataGenerator mSimulationDataGenerator;
	bool mSimulationInitilized;

	//Serial analysis vars:
	U32 mSampleRateHz;
	U32 mByteNumber;
	U64 mCurrentCommand;
	U64 mCurrentTarget;
	std::vector<U64> mArrowLocataions;

#pragma warning( pop )
};

extern "C" ANALYZER_EXPORT const char* __cdecl GetAnalyzerName();
extern "C" ANALYZER_EXPORT Analyzer* __cdecl CreateAnalyzer( );
extern "C" ANALYZER_EXPORT void __cdecl DestroyAnalyzer( Analyzer* analyzer );

#endif //SERIAL_ANALYZER_H
