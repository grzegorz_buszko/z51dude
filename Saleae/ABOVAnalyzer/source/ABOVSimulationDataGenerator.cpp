/*
 * This Saleae Analyzer is based on the Saleae I2C Analyzer Code which is 
 * available from <https://support.saleae.com/saleae-api-and-sdk/protocol-analyzer-sdk>.
 * The modifications for capturing, decoding and analyzing the ABOV OCD protocol have
 * been made by Thomas Ries <tries@gmx.net>.
 */

#include "ABOVSimulationDataGenerator.h"


ABOVSimulationDataGenerator::ABOVSimulationDataGenerator()
{
}

ABOVSimulationDataGenerator::~ABOVSimulationDataGenerator()
{
}

void ABOVSimulationDataGenerator::Initialize( U32 simulation_sample_rate, ABOVAnalyzerSettings* settings )
{
	mSimulationSampleRateHz = simulation_sample_rate;
	mSettings = settings;

	mClockGenerator.Init( 400000, simulation_sample_rate );

	mSda = mABOVSimulationChannels.Add( settings->mSdaChannel, mSimulationSampleRateHz, BIT_LOW );
	mScl = mABOVSimulationChannels.Add( settings->mSclChannel, mSimulationSampleRateHz, BIT_LOW );

	mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 10.0 ) ); //insert 10 bit-periods of idle

	mValue = 0;
}

U32 ABOVSimulationDataGenerator::GenerateSimulationData( U64 largest_sample_requested, U32 sample_rate, SimulationChannelDescriptor** simulation_channels )
{
	U64 adjusted_largest_sample_requested = AnalyzerHelpers::AdjustSimulationTargetSample( largest_sample_requested, sample_rate, mSimulationSampleRateHz );

	while( mScl->GetCurrentSampleNumber() < adjusted_largest_sample_requested )
	{
		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 500 ) );

		// 13,13,13
		CreateStartSequence();

		CreateStart();
		CreateABOVByte( 0x12, ABOV_NAK );
		CreateABOVByte( 0x20, ABOV_NAK );
		CreateABOVByte( 0x04, ABOV_NAK );
		CreateABOVByte( 0x00, ABOV_NAK );
		CreateABOVByte( 0x00, ABOV_NAK );
		CreateABOVByte( 0x80, ABOV_NAK );
		CreateStop();

		CreateStart();
		CreateABOVByte( 0x12, ABOV_NAK );
		CreateABOVByte( 0x21, ABOV_NAK );
		CreateABOVByte( 0x00, ABOV_NAK );
		CreateABOVByte( 0x00, ABOV_NAK );
		CreateABOVByte( 0x00, ABOV_NAK );
		CreateABOVByte( 0x80, ABOV_NAK );
		CreateStop();

		CreateStart();
		CreateABOVByte( 0x12, ABOV_NAK );
		CreateABOVByte( 0x20, ABOV_NAK );
		CreateABOVByte( 0x04, ABOV_NAK );
		CreateABOVByte( 0x00, ABOV_NAK );
		CreateABOVByte( 0x00, ABOV_NAK );
		CreateABOVByte( 0x90, ABOV_NAK );
		CreateStop();

		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 100 ) );

		//DSCL 1->0 (Device wait??)
		mScl->TransitionIfNeeded( BIT_LOW );
		//Wait 1.25ms
		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 10 ) );

		//DSCL 0->1 (Device ready??)
		mScl->TransitionIfNeeded( BIT_HIGH );
		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 0.5 ) );

		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 100 ) );

	}

	*simulation_channels = mABOVSimulationChannels.GetArray();
	return mABOVSimulationChannels.GetCount();
}

void ABOVSimulationDataGenerator::CreateABOVByte( U8 data, ABOVResponse reply )
{
	U8 parity;
	BitState bit;
	if( mScl->GetCurrentBitState() == BIT_HIGH )
	{
		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 1.0 ) );
		mScl->Transition();
		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 1.0 ) );
	}

	BitExtractor bit_extractor( data, AnalyzerEnums::LsbFirst, 8 );

	for( U32 i=0; i<8; i++ )
	{
		bit=bit_extractor.GetNextBit();
		CreateBit( bit );
		parity += (bit == BIT_HIGH)? 1:0;
	}

	//&&& create parity bit
	CreateBit( ((parity % 2) == 1)? BIT_HIGH: BIT_LOW );


	if( reply == ABOV_ACK )
		CreateBit( BIT_LOW );
	else
		CreateBit( BIT_HIGH );

	mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 4.0 ) );
}

void ABOVSimulationDataGenerator::CreateBit( BitState bit_state )
{
	if( mScl->GetCurrentBitState() != BIT_LOW )
		AnalyzerHelpers::Assert( "CreateBit expects to be entered with scl low" );

	mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 0.5 ) );

	mSda->TransitionIfNeeded( bit_state );

	mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 0.5 ) );

	mScl->Transition(); //posedge

	mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 1.0 ) );

	mScl->Transition(); //negedge
}

void ABOVSimulationDataGenerator::CreateStart()
{
	mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 1.0 ) );

	//1st, we need to make SDA high, 
	SafeChangeSda( BIT_HIGH );

	//2nd, we need make the clock high.
	if( mScl->GetCurrentBitState() == BIT_LOW )
	{
		mScl->Transition();
		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 1.0 ) );
	}

	//3rd, bring SDA high.
	mSda->Transition();
	mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 1.0 ) );
}


void ABOVSimulationDataGenerator::CreateStop()
{
	mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 1.0 ) );

	//1st, we need to make SDA low, 
	SafeChangeSda( BIT_LOW );

	//2nd, we need make the clock high.
	if( mScl->GetCurrentBitState() == BIT_LOW )
	{
		mScl->Transition();
		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 1.0 ) );
	}

	//3rd, bring SDA high.
	mSda->Transition();
	mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 1.0 ) );
}

void ABOVSimulationDataGenerator::CreateRestart()
{
	CreateStart();
}

void ABOVSimulationDataGenerator::SafeChangeSda( BitState bit_state )
{
	if( mSda->GetCurrentBitState() != bit_state )
	{
		//make sure SCK is low before we toggle it
		if( mScl->GetCurrentBitState() == BIT_HIGH )
		{
			mScl->Transition();
			mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 1.0 ) );
		}

		mSda->Transition();
		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 1.0 ) );
	}	
}

void ABOVSimulationDataGenerator::CreateStartSequence( )
{
	// set SCL high, wait 10us
	mScl->TransitionIfNeeded( BIT_HIGH );
	mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 1.0 ) );


	// 13 toggles l->h on SDA, stays H at the end
	for (U8 i=0; i< 13; i++) {
		mSda->TransitionIfNeeded( BIT_LOW );
		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 0.5 ) );
		mSda->TransitionIfNeeded( BIT_HIGH );
		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 0.5 ) );
	}

	// 13 toggles l->h on SCL, stays H at the end
	for (U8 i=0; i< 13; i++) {
		mScl->TransitionIfNeeded( BIT_LOW );
		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 0.5 ) );
		mScl->TransitionIfNeeded( BIT_HIGH );
		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 0.5 ) );
	}

	// 13 toggles l->h on SDA, stays H at the end
	for (U8 i=0; i< 13; i++) {
		mSda->TransitionIfNeeded( BIT_LOW );
		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 0.5 ) );
		mSda->TransitionIfNeeded( BIT_HIGH );
		mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 0.5 ) );
	}

	mABOVSimulationChannels.AdvanceAll( mClockGenerator.AdvanceByHalfPeriod( 1.0 ) );

}
