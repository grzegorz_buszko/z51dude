/*
 * This Saleae Analyzer is based on the Saleae I2C Analyzer Code which is 
 * available from <https://support.saleae.com/saleae-api-and-sdk/protocol-analyzer-sdk>.
 * The modifications for capturing, decoding and analyzing the ABOV OCD protocol have
 * been made by Thomas Ries <tries@gmx.net>.
 */

#ifndef ABOV_ANALYZER_SETTINGS
#define ABOV_ANALYZER_SETTINGS

#include <AnalyzerSettings.h>
#include <AnalyzerTypes.h>

//enum ABOVDirection { ABOV_READ, ABOV_WRITE };
enum ABOVResponse { ABOV_ACK, ABOV_NAK };

enum AddressDisplay { NO_DIRECTION_7, NO_DIRECTION_8, YES_DIRECTION_8 };

class ABOVAnalyzerSettings : public AnalyzerSettings
{
public:
	ABOVAnalyzerSettings();
	virtual ~ABOVAnalyzerSettings();
	
	virtual bool SetSettingsFromInterfaces();
	virtual void LoadSettings( const char* settings );
	virtual const char* SaveSettings();

	void UpdateInterfacesFromSettings();

	Channel mSdaChannel;
	Channel mSclChannel;

protected:
	std::auto_ptr< AnalyzerSettingInterfaceChannel > mSdaChannelInterface;
	std::auto_ptr< AnalyzerSettingInterfaceChannel > mSclChannelInterface;
};

#endif //ABOV_ANALYZER_SETTINGS
