# fxload

fxload utility that allows uploading firmware images to Cypress 
EZ-USB devices.

This version of fxload has been anhanced to support writing VID/PID 
values to the external I2C EEPROM of the Cypress device, as well as
reading and dumping the content of this I2C EEPROM.

proudly cloned from https://github.com/esden/fxload.git and enhanced
to support reading the internal EEPROM.

# fxload -t fx2lp -s Vend_Ax.hex -D /proc/bus/usb/002/007 -r 64
read the first 64 bytes of the attached EEPROM.
