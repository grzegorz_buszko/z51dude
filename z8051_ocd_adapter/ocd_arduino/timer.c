/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdint.h>
#include <avr/io.h>
#include <util/atomic.h>
#include <avr/interrupt.h>

// some F_CPU magic to determine PRESCALER & DIV1 for 1ms timer
#if F_CPU == 16000000L
    // 16MHz * 0.001s -> 16000 cycles (64*250)
    #define PRESCALER   ( (1<<CS00) | (1<<CS01) )       // 64
    #define DIV1        250                             // 250
#else
    #error "please define PRESCALER and DIV1 for the configured F_CPU"
#endif

#define TMR_RELOAD  (256-DIV1)

static uint64_t    millisecond_counter;

void timer_init(void) {
    // use timer0 as 1ms counter
    // initialize counter
    TCNT0 = TMR_RELOAD;

    // set up normal timer mode  with presacaler as calculated above
    TCCR0A  = 0;
    TCCR0B  = PRESCALER;
 
    // clear overflow bit
    TIFR0  |= (1<<TOV0);
    // enable timer INT
    TIMSK0 |= (1<<TOIE0);

    // enable interrupts (global)
    sei();

}


void timer_clear_milliseconds(void) {
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        millisecond_counter=0;
    }
}

uint64_t timer_get_miliseconds(void) {
    uint64_t ms;

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        ms=millisecond_counter;
    }
    return ms;
}

ISR(TIMER0_OVF_vect) {
    TCNT0=TMR_RELOAD;
    millisecond_counter++;
}
