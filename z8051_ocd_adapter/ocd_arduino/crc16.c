/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "crc16.h"

// calculate CRC16 across a buffer (byte-wise)
short unsigned int crc16_byte(unsigned char *buff, int len) {
    short unsigned int crc=0;
    short int i;
    for (i=0; i < len; i++) {
        crc = crc16_update (crc, (short unsigned int)buff[i], 8);
    }
    return crc;
}



// calculate one CRC16 iteration with <bitlen> data
short unsigned int crc16_update(short unsigned int crc, short unsigned int data, unsigned char bitlen) {
    int i;

    crc = crc ^ ((short unsigned int)data << (16-bitlen));
    for (i=0; i<bitlen; i++) {
        if (crc & 0x8000) {
            crc = (crc << 1) ^ 0x1021;
        } else {
            crc <<= 1;
        }
    }
    return crc;
}
