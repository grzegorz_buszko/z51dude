/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "timer.h"

#define BUFLEN  64
static uint8_t rxbuf[BUFLEN];
static volatile uint8_t rxbuf_w=0;       // points to next buffer position to write into
static uint8_t rxbuf_r=0;       // points to next buffer position to read from
static uint16_t timeout=0;      // timeout when reading a byte from buffer and waiting for RX

void serial_begin(uint32_t speed) {
    uint16_t prescaler;
    speed=115200;
    prescaler = (((F_CPU / (speed * 8L))) - 1); // at double speed

    // set prescaler for baudrate
    UBRR0H = (prescaler >> 8);
    UBRR0L = prescaler;
    // double speed
    UCSR0A |= (1<< U2X0);
    // enable RX/TX circuits, RX interrupt enable
    UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0 );
    // set frame format 8/1/N
    UCSR0C = (1<<UCSZ00) | (1<<UCSZ01);
}

void serial_settimeout(uint16_t msec) {
    timeout=msec;
}
  
void serial_write(uint8_t *buffer, uint8_t len) {
    uint8_t i;
    
    for (i=0; i < len; i++) {
        while ((UCSR0A & (1<<UDRE0))==0) {}
        UDR0 = buffer[i];
    }
}

uint8_t serial_readBytes(uint8_t *buffer, uint8_t len) {
    uint64_t s_time;
    uint8_t  i;
    
    // got data in buffer
    for (i=0; i<len; i++) {
        // if buffer empty, wait for more
        s_time = timer_get_miliseconds();
        while (rxbuf_r == rxbuf_w) {
            if ( (s_time+timeout) < timer_get_miliseconds() ) { return i; }
        }
        // read a byte
        buffer[i] = rxbuf[rxbuf_r];
        rxbuf_r++;
        if (rxbuf_r >= BUFLEN) { rxbuf_r = 0; }
    }

    return i;
}



ISR(USART_RX_vect) {
    rxbuf[rxbuf_w] = UDR0;
    rxbuf_w++;
    if (rxbuf_w >= BUFLEN) { rxbuf_w = 0; }
}

