/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include "digitalio.h"
#include "serial.h"
#include "timer.h"

#include "crc16.h"

// AVR defs
// clock
#define DSCLpin_DDR     DDRB
#define DSCLpin_PORT    PORTB
#define DSCLpin_PIN     PINB
#define DSCLpin         PB2         // D10
// data
#define DSDApin_DDR     DDRB
#define DSDApin_PORT    PORTB
#define DSDApin_PIN     PINB
#define DSDApin         PB3         // D11
// power control
#define PWRpin_DDR      DDRB
#define PWRpin_PORT     PORTB
#define PWRpin_PIN      PINB
#define PWRpin          PB1         // D9
// power sense
#define Q5V_DDR         DDRD
#define Q5V_PORT        PORTD
#define Q5V_PIN         PIND
#define Q5V             PD2         // D2
//
#define LED_DDR         DDRB
#define LED_PORT        PORTB
#define LED_PIN         PINB
#define LED             PB5         // D14 (onboard LED)




#define MAXBUF              0x4f
#define OK                  0x80
#define NOK                 0x81
#define SERTIMEOUT          2000    // timeout serial interface read 2000ms

#define CMD_ECHO              0x00
#define CMD_RESET_PWR_ARDUINO 0x01
#define CMD_RESET_PWR_EXTERN  0x02
#define CMD_Z8051_CMD         0x03

#define OCD_CMD_WRITE_CONT    0x10
#define OCD_CMD_WRITE         0x12
#define OCD_CMD_READ_CONT     0x20
#define OCD_CMD_READ          0x22

#define OCD_DATA_CODE         0x10  // Z51F3220, Code Area
#define OCD_DATA_XDATA        0x11  // Z51F3220, OCD Data Area in the range 0x8000–0x803F
#define OCD_DATA_SFR          0x13  // Z51F3220, Special Function Registers
#define OCD_DATA_BDC          0x20  // Z51F3220, Background Debugging Controller
#define OCD_DATA_DBG          0x21  // Z51F3220, Debugging Logic
#define OCD_DATA_TRG          0x22  // Z51F3220, Debugging Trigger

# define DLY_DSCL             4     // delay in us to be introduced after
                                    // toggling the DSCL (clock) line


void setup(void);
void loop(void);

int unused(void);
static inline void wrbitDSCL(uint8_t b) __attribute__((always_inline));
static inline void wrbitDSDA(uint8_t b) __attribute__((always_inline));
void initSequence(void);
unsigned short int readCpuId(void);
int send_data_frame(unsigned char *wrbuffer, unsigned char count);
int recv_data_frame(unsigned char* rdbuffer, unsigned char max_rdbuffer_size);

void sndstartbit(void);
void sndstopbit(void);
unsigned char rdbyte(void);
unsigned char wrbyte(unsigned char sb);


unsigned char head[4]={0x68,0x00,0x00,0x68};
unsigned char rdbuffer[MAXBUF];
unsigned char wrbuffer[MAXBUF];


int main(void) {
    setup();
    
    while (1) {
        loop();
    }
}


void setup(void) {
  timer_init();

  serial_begin(115200); 
  serial_settimeout(100);
  
  digitalWrite(&DSCLpin_PORT, DSCLpin, LOW);
  digitalWrite(&DSDApin_PORT, DSDApin, LOW);

  pinMode(&DSCLpin_DDR, DSCLpin, INPUT);
  pinMode(&DSDApin_DDR, DSDApin, INPUT);
  pinMode(&Q5V_DDR, Q5V, INPUT);
  pinMode(&LED_DDR, LED, OUTPUT);

  wrbitDSDA(0);
  wrbitDSCL(0);
  
  pinMode(&PWRpin_DDR, PWRpin, OUTPUT);
  digitalWrite(&PWRpin_PORT, PWRpin, LOW);  

  wrbitDSCL(1);
  
}

void loop(void) {
  
  int recv_data_len;
  int i;
  unsigned long timeout_timer;
  unsigned short int cpuid;
  unsigned char cmd;
  unsigned char count;
  
  do {
    //loop
    recv_data_len=recv_data_frame(rdbuffer, MAXBUF);
  } while (recv_data_len < 0);

  cmd=rdbuffer[0];
  
  // turn LED on (signal busy)
  digitalWrite(&LED_PORT, LED, HIGH);

  switch(cmd) {
    case CMD_ECHO:
      // echo --> send back
      send_data_frame(rdbuffer, recv_data_len);
      break;
      
    case CMD_RESET_PWR_ARDUINO:
      // reset device with power on off
      digitalWrite(&PWRpin_PORT, PWRpin, HIGH);  
      // 100ms off pulse duration
      _delay_ms(100);
      digitalWrite(&PWRpin_PORT, PWRpin, LOW);  
      initSequence();
      _delay_ms(1);
      cpuid=readCpuId();
      wrbuffer[0]=OK;
      wrbuffer[1]=(unsigned char) (cpuid & 0xff);
      wrbuffer[2]=(unsigned char) (cpuid >> 8);
      send_data_frame(wrbuffer,3);
      break;
      
    case CMD_RESET_PWR_EXTERN:
      // reset device when power on pin 
      timeout_timer = timer_get_miliseconds();
//entprellen??
      while(digitalRead(&Q5V_PIN, Q5V)==0) {
        if(timer_get_miliseconds() - timeout_timer > 10000)   break;
      }; 
      initSequence();
      _delay_ms(1);
      cpuid=readCpuId();
      wrbuffer[0]=OK;
      wrbuffer[1]=(unsigned char) (cpuid & 0xff);
      wrbuffer[2]=(unsigned char) (cpuid >> 8);
      send_data_frame(wrbuffer,3);
      break;
    
    case CMD_Z8051_CMD:
      switch(rdbuffer[1]) {
         case OCD_CMD_WRITE:
            // byte 1 Write command
            // byte 2 Code area
            // byte 3 Addresses 7:0
            // byte 4 Addresses 15:8
            // byte 5 Addresses 24:16
            // byte 6 Data
            // same code CMD_Z8051_WRITE_CONT
         case OCD_CMD_WRITE_CONT:
            // byte 1 Write Cont command
            // byte 2.... Data
            sndstartbit();  
            for(i=1;i<recv_data_len;i++) {
               wrbyte(rdbuffer[i]);
            }
            wrbuffer[0]=OK;
      
            sndstopbit();
            send_data_frame(wrbuffer,1);
            break;
      
         case OCD_CMD_READ:
            // byte 1 Read command
            // byte 2 Code area
            // byte 3 Addresses 7:0
            // byte 4 Addresses 15:8
            // byte 5 Addresses 24:16
            // byte 6 Count
            sndstartbit();  
      
            for(i=1;i<=6;i++) {
              // lcd.print(rdbuffer[i],16);
              wrbyte(rdbuffer[i]);
            }
            count=rdbuffer[6];
            wrbuffer[0]=OK;
            for(i=1; i<=count;i++) {
              wrbuffer[i] = rdbyte();
            }
            sndstopbit();
      
            send_data_frame(wrbuffer,count+1);
            break;

         case OCD_CMD_READ_CONT:
            // byte 1 Read Cont command 0x20 =CD_CMD_READ_CONT
            // byte 2 Count
            sndstartbit();  
      
            for(i=1;i<=2;i++) {
              wrbyte(rdbuffer[i]);
            }
            count=rdbuffer[2];
            wrbuffer[0]=OK;
            for(i=1; i<=count;i++) {
               wrbuffer[i] = rdbyte();
            }
      
            sndstopbit();
            send_data_frame(wrbuffer,count+1);
      }
    break;
      
    default:
      break;
  } 

  // work done, turn LED off
  digitalWrite(&LED_PORT, LED, LOW);

}



int send_data_frame(unsigned char *wrbuffer, unsigned char count){
  unsigned short int crc;
  
  head[0]=0x68;
  head[1]=count;
  head[2]=count;
  head[3]=0x68;

  serial_write(head, sizeof(head)) ;
  serial_write(wrbuffer, count) ;

  crc=crc16_byte(wrbuffer, count);
  serial_write((unsigned char*)&crc, sizeof(crc)) ;
  
  return 0;  
}

int recv_data_frame(unsigned char* rdbuffer, unsigned char max_rdbuffer_size) {

  int rdlen;
  int head_idx;
  int buffer_idx;
  int buffer_len=0;
  unsigned short int crc_rcv;
  unsigned short int crc_frm;
  unsigned long s_time;         // time in milliseconds since start
  
  head_idx=0;
  
  s_time=timer_get_miliseconds();
  while (head_idx < 4) {
    rdlen=serial_readBytes(&rdbuffer[head_idx], 1);
    if (rdlen > 0) {
      // lcd.print(rdbuffer[head_idx],16);
      switch (head_idx) {
        case 0:
          if (rdbuffer[head_idx] == 0x68) {
            head_idx++;
          }
          break;
        case 1:
          buffer_len = rdbuffer[head_idx];
          if(max_rdbuffer_size < buffer_len+2) {
            // buffer to small
            return -4;
          }
          head_idx++;
          break;
        case 2:
          if (rdbuffer[head_idx] == buffer_len) {
            head_idx++;
          } else {
            head_idx=0;
          }
         break;
        case 3:
          if (rdbuffer[head_idx] == 0x68) {
            head_idx ++;
          } else {
            head_idx=0;
          }
          break;
        default:
          // should never come here in
          return -2;
      }
    } else {
      // check for timeout
      if((timer_get_miliseconds() - s_time) > SERTIMEOUT) {
        //timeout
        return -1;
      }
    }
  }
  buffer_idx=0;
  s_time=timer_get_miliseconds();

  while (buffer_idx < buffer_len+2) { // with crc
    rdlen=serial_readBytes(&rdbuffer[buffer_idx], 1) ;
    if (rdlen > 0) {
      buffer_idx++;
    }
    // check for timeout
    if(timer_get_miliseconds() - s_time > SERTIMEOUT) {
      //timeout
      return -1;
    }
  }

  crc_rcv = crc16_byte(rdbuffer, buffer_len);
  crc_frm = rdbuffer[buffer_len] + (rdbuffer[buffer_len+1] <<8);

  if (crc_rcv != crc_frm) {
    // crc NOK
    return -3;
  }
  // crc ok
  return(buffer_len);
}

  

unsigned short int readCpuId(void) {
  unsigned int id;
  
  sndstartbit();  
  wrbyte(OCD_CMD_READ); // Read command
  wrbyte(OCD_DATA_BDC); // Code area
  wrbyte(0x00); // Addresses 7:0
  wrbyte(0x00); // Addresses 15:8
  wrbyte(0x00); // Addresses 24:16
  wrbyte(0x02); // Count
  id=rdbyte();
  id=id | rdbyte()<<8;
  sndstopbit();

  return(id);
}



// 10-bit packet transmission using two-pin interface.
// 1-packet consists of 8-bit data, 1-bit parity and 1-bit acknowledge.
// Parity is even of ‘1’ for 8-bit data in transmitter.
// Receiver generates acknowledge bit as ‘0’ when transmission for 8-bit data and its parity has no error.
// When transmitter has no acknowledge (Acknowledge bit is ‘1’ at tenth clock), error process is executed in transmitter.
// When acknowledge error is generated, host PC makes stop condition and transmits command which has error again.



void sndstartbit(void) {
  // Start

  // DSCL high
  // if not wait until high
  // implement here???
  
  wrbitDSDA(0);
  wrbitDSCL(0);
}

void sndstopbit(void) {
  // Start

  // DSCL high
  // if not wait until high
  // implement here???

  wrbitDSDA(0);
  wrbitDSCL(1);
  wrbitDSDA(1);
}


unsigned char rdbyte(void) {
  int nBit;
  int paritycount;
//&&&  unsigned char ack;
  unsigned char rb;

  rb=0;
  paritycount=0;

  // send data bits
  for ( nBit=0; nBit < 8; nBit++) {
      wrbitDSCL(1);
      _delay_us(DLY_DSCL);
      if (digitalRead(&DSDApin_PIN, DSDApin)) {
          paritycount++;
          rb= rb | (1u << nBit);
      } 
      wrbitDSCL(0);
      _delay_us(DLY_DSCL);
  }

  // parity
  wrbitDSCL(1);
  _delay_us(DLY_DSCL);
       
  if (digitalRead(&DSDApin_PIN, DSDApin) == (paritycount % 2)) {
      //parity ok
  }

  wrbitDSCL(0);
  _delay_us(DLY_DSCL);
  
  // ACK
  wrbitDSDA(0); // alwas ACK as OK...
  wrbitDSCL(1);
  _delay_us(DLY_DSCL);
  wrbitDSCL(0);
  wrbitDSDA(1);

  return(rb);   
}



/*
 *  Write Byte
 * 
 */
unsigned char wrbyte(unsigned char sb) {
  int nBit;
  int paritycount;
  unsigned char ack;
  
  paritycount=0;

  // send data bits
  for ( nBit=0; nBit < 8; nBit++) {
      if ((1u << nBit) & sb) {
          paritycount++;
      }
      wrbitDSDA( (1u << nBit) & sb); 
      wrbitDSCL(1);
      _delay_us(DLY_DSCL);
      wrbitDSCL(0);
      _delay_us(DLY_DSCL);
  }

  // send parity bit
  if (paritycount % 2) {
      wrbitDSDA(1);
  } else {
      wrbitDSDA(0);
  }
  wrbitDSCL(1);
  _delay_us(DLY_DSCL);
  wrbitDSCL(0);
  _delay_us(DLY_DSCL);
  
  wrbitDSDA(1);
  wrbitDSCL(1);
  _delay_us(DLY_DSCL);
  // check Ack
  ack = digitalRead(&DSDApin_PIN, DSDApin);  
  wrbitDSCL(0);
  _delay_us(DLY_DSCL);
  
  return(ack);   
}

void initSequence(void) {
  int i=0;

  _delay_ms(8);
  
  for (i=1; i<=13;i++) {
      wrbitDSDA(0);
      _delay_us(DLY_DSCL);
      wrbitDSDA(1);
      _delay_us(DLY_DSCL);
  }

  for (i=1; i<=13;i++) {
      wrbitDSCL(0);
      _delay_us(DLY_DSCL);
      wrbitDSCL(1);
      _delay_us(DLY_DSCL);
  }
  
  for (i=1; i<=13;i++) {
      wrbitDSDA(0);   
      _delay_us(DLY_DSCL);
      wrbitDSDA(1);
      _delay_us(DLY_DSCL);
  }

  _delay_ms(1);

  sndstartbit();  
  wrbyte(0x12); // Write command
  wrbyte(0x20); // Background Debugging Controller
  wrbyte(0x04); // Addresses 7:0
  wrbyte(0x00); // Addresses 15:8
  wrbyte(0x00); // Addresses 24:16
  wrbyte(0x80); // Data
  sndstopbit();  
  // delayMicroseconds(120);  
  
  sndstartbit();  
  wrbyte(0x12); // Write command
  wrbyte(0x21); // Debugging Logic
  wrbyte(0x00); // Addresses 7:0
  wrbyte(0x00); // Addresses 15:8
  wrbyte(0x00); // Addresses 24:16
  wrbyte(0x80); // Data
  sndstopbit();  
  // delayMicroseconds(120);  
  
  sndstartbit();// equence to the device to enter Debug Mode:
  wrbyte(0x12); // Write command
  wrbyte(0x21); // Debugging Logic
  wrbyte(0x00); // Addresses 7:0
  wrbyte(0x00); // Addresses 15:8
  wrbyte(0x00); // Addresses 24:16
  wrbyte(0x90); // Data
  sndstopbit();  

  // was 220ms -> too long...
  _delay_ms(20);

  sndstartbit();// 
  wrbyte(0x12); // Write command
  wrbyte(0x20); // Background Debugging Controller
  wrbyte(0x00); // Addresses 7:0
  wrbyte(0x00); // Addresses 15:8
  wrbyte(0x00); // Addresses 24:16
  wrbyte(0x00); // Data
  sndstopbit();  
  
}

static inline void wrbitDSCL(uint8_t b) {
  if ( b == 0 ) {
    // Low
    pinMode(&DSCLpin_DDR, DSCLpin, OUTPUT);
  } else {
    // High because of 10k pull-up resistors
    pinMode(&DSCLpin_DDR, DSCLpin, INPUT);
  }
}

static inline void wrbitDSDA(uint8_t b) {
  if ( b == 0 ) {
    // Low
    pinMode(&DSDApin_DDR, DSDApin, OUTPUT);
  } else {
    // High because of 10k pull-up resistors
    pinMode(&DSDApin_DDR, DSDApin, INPUT);
  }
}
