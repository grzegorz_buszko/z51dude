/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdint.h>
#include <avr/io.h>
#include "digitalio.h"

// no code here-> inline functions defined in .h 
#if 0
void pinMode(volatile uint8_t *ddr, uint8_t bit, uint8_t direction) {

    if (direction == INPUT) {
        *ddr &= ~(1<<(bit));
    } else {
        *ddr |= (1<<(bit));
    }

}

void digitalWrite(volatile uint8_t *port, uint8_t bit, uint8_t state) {
    if (state == LOW) {
        *port &= ~(1<<(bit));
    } else {
        *port |= (1<<(bit));
    }
}

uint8_t digitalRead(volatile uint8_t *pin, uint8_t bit) {
    return ((*pin) & (1<<(bit)))? 1:0;
}
#endif
