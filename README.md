# z51dude

An Z51 / ABOV open source programmer

This project aims to reverse engineer the ZILOG Z51 / ABOV MC96F* OCD 
protocol with the goal to implement a simple open source OCD-I programmer
for this device family.


Saleae/             - Saleae Logic plug-in for decoding the OCD protocol
test/               - test software and test firmware
tools/              - additional tools, not mandatory for the operation
                      of z51dude, but handy if playing around
z8051_dude/         - PC side programmer software, communicates with the 
                      OCD adapter via USB (serial over USB).
ocd_adapter/        - OCD adapter firmware for supported hardware
    ocd_arduino/    - Firmware for Arduino Nano



Contact information of the developers can be found in the AUTHORS file.

The list of supported/tested devices is currently limited to devices we 
have access to. Supplying a few MCU hardware samples to the developers 
may get a new MCUs added quickly - please contact us.

 
IRC: Libera.Chat #z51dude
