/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

//***
//*** OCD command definitions
//***
// some command sequences are fixed an can be run in a loop, others
// include variable data or data buffers.
typedef struct {
    int length;
    unsigned char *seq;
} cmdseq_t;

//
// program flash page
unsigned char ocd_cmd_pgm_page1[]   = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x00 };
unsigned char ocd_cmd_pgm_page2[]   = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xfe, 0x00, 0x00, 0x01 };
unsigned char ocd_cmd_pgm_page3[]   = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_XDATA,    0x00, 0x80, 0x00 };
unsigned char ocd_cmd_pgm_page4[]   = { CMD_Z8051_CMD, OCD_CMD_WRITE_CONT };
unsigned char ocd_cmd_pgm_page5[]   = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xfa, 0x00, 0x00, 0x00, 0, 0, 0xa5, 0x03 };
unsigned char ocd_cmd_pgm_page6[]   = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_SFR,      0xfe, 0x00, 0x00, 0x01 };
//
// run program (same sequence on MC96F6432 and MC95FG104)
unsigned char ocd_cmd_runpgm1[]     = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_TRG,      0x01, 0x00, 0x00, 0x28, 0x88, 0xa6, 0x00,
                                        0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46, 0x37, 0x00, 
                                        0x00, 0x00, 0x00, 0x00, 0x00}; // unnown
unsigned char ocd_cmd_runpgm2[]     = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_TRG,      0x19, 0x00, 0x00, 0x00 };
unsigned char ocd_cmd_runpgm3[]     = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_TRG,      0x00, 0x00, 0x00, 0x88 };
unsigned char ocd_cmd_runpgm4[]     = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_DBG,      0x01, 0x00, 0x00, 0x00 };
unsigned char ocd_cmd_runpgm5[]     = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_DBG,      0x02, 0x00, 0x00, 0x00, 0x00, 0x00 }; // write PC 0x0000
unsigned char ocd_cmd_runpgm6[]     = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_TRACER,   0x00, 0x00, 0x00, 0x80 };
unsigned char ocd_cmd_runpgm7[]     = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_DBG,      0x00, 0x00, 0x00, 0x80 };
unsigned char ocd_cmd_runpgm8[]     = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_DBG,      0x00, 0x00, 0x00, 0xc0 };
// run program from 0x000 (set PC)
cmdseq_t ocd_cmdseq_runpgm[] = {
//    { sizeof(ocd_cmd_runpgm1), ocd_cmd_runpgm1 }, not needed?
    { sizeof(ocd_cmd_runpgm2), ocd_cmd_runpgm2 },
    { sizeof(ocd_cmd_runpgm3), ocd_cmd_runpgm3 },
    { sizeof(ocd_cmd_runpgm4), ocd_cmd_runpgm4 },
    { sizeof(ocd_cmd_runpgm5), ocd_cmd_runpgm5 },
    { sizeof(ocd_cmd_runpgm6), ocd_cmd_runpgm6 },
    { sizeof(ocd_cmd_runpgm7), ocd_cmd_runpgm7 },
    { sizeof(ocd_cmd_runpgm8), ocd_cmd_runpgm8 },
};
// continue execution (without touching PC)
cmdseq_t ocd_cmdseq_contpgm[] = {
    // don't need the unknown magic in TRG
    { sizeof(ocd_cmd_runpgm2), ocd_cmd_runpgm2 },
    { sizeof(ocd_cmd_runpgm3), ocd_cmd_runpgm3 },
    { sizeof(ocd_cmd_runpgm4), ocd_cmd_runpgm4 },
    // don't set the PC
    { sizeof(ocd_cmd_runpgm6), ocd_cmd_runpgm6 },
    { sizeof(ocd_cmd_runpgm7), ocd_cmd_runpgm7 },
    { sizeof(ocd_cmd_runpgm8), ocd_cmd_runpgm8 },
};
//
// stop program (different that OCD debugger) same as OCD Reset
unsigned char ocd_cmd_stoppgm1[]    = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_DBG,      0x01, 0x00, 0x00, 0x80 };
unsigned char ocd_cmd_stoppgm2[]    = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_DBG,      0x00, 0x00, 0x00, 0x90 };
unsigned char ocd_cmd_stoppgm3[]    = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_DBG,      0x01, 0x00, 0x00, 0x00 };
cmdseq_t ocd_cmdseq_stoppgm[] = {
    { sizeof(ocd_cmd_stoppgm1), ocd_cmd_stoppgm1 },
    { sizeof(ocd_cmd_stoppgm2), ocd_cmd_stoppgm2 },
    { sizeof(ocd_cmd_stoppgm3), ocd_cmd_stoppgm3 },
};
// single step program 
unsigned char ocd_cmd_steppgm1[]    = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_DBG,      0x01, 0x00, 0x00, 0x20 };
unsigned char ocd_cmd_steppgm2[]    = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_TRG,      0x00, 0x00, 0x00, 0x00 };
unsigned char ocd_cmd_steppgm3[]    = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_TRACER,   0x00, 0x00, 0x00, 0x80 };
unsigned char ocd_cmd_steppgm4[]    = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_DBG,      0x00, 0x00, 0x00, 0x80 };
unsigned char ocd_cmd_steppgm5[]    = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_DBG,      0x00, 0x00, 0x00, 0xE0 };
cmdseq_t ocd_cmdseq_steppgm[] = {
    { sizeof(ocd_cmd_steppgm1), ocd_cmd_steppgm1 },
    { sizeof(ocd_cmd_steppgm2), ocd_cmd_steppgm2 },
    { sizeof(ocd_cmd_steppgm3), ocd_cmd_steppgm3 },
    { sizeof(ocd_cmd_steppgm4), ocd_cmd_steppgm4 },
    { sizeof(ocd_cmd_steppgm5), ocd_cmd_steppgm5 },
};
//
// OCD device reset
// Reverse engineered with logic analyzer by using the Zilog OCD2_dbg_VS2010_Zilog.exe v2.2 software
unsigned char ocd_cmd_dev_reset1[]  = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_DBG,      0x01, 0x00, 0x00, 0x80 };
unsigned char ocd_cmd_dev_reset2[]  = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_DBG,      0x00, 0x00, 0x00, 0x90 };
unsigned char ocd_cmd_dev_reset3[]  = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_DBG,      0x01, 0x00, 0x00, 0x00 };
cmdseq_t ocd_cmdseq_reset[] = {
    { sizeof(ocd_cmd_dev_reset1), ocd_cmd_dev_reset1 },
    { sizeof(ocd_cmd_dev_reset2), ocd_cmd_dev_reset2 },
    { sizeof(ocd_cmd_dev_reset3), ocd_cmd_dev_reset3 },
};
//
// flash bulk erase (code)
unsigned char ocd_cmd_bulk_erase1[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x00 };
unsigned char ocd_cmd_bulk_erase2[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xfe, 0x00, 0x00, 0x01 };
unsigned char ocd_cmd_bulk_erase3[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_XDATA,    0x00, 0x80, 0x00 };
unsigned char ocd_cmd_bulk_erase4[] = { CMD_Z8051_CMD, OCD_CMD_WRITE_CONT };
unsigned char ocd_cmd_bulk_erase5[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x01 };
unsigned char ocd_cmd_bulk_erase6[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xfd, 0x00, 0x00, 0xa5 };
unsigned char ocd_cmd_bulk_erase7[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xfe, 0x00, 0x00, 0x02 };
unsigned char ocd_cmd_bulk_erase8[] = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_SFR,      0xfe, 0x00, 0x00, 0x01 };
//
// OPTION page erase
unsigned char ocd_cmd_optn_erase1[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x00 };
unsigned char ocd_cmd_optn_erase2[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xfe, 0x00, 0x00, 0x01 };
unsigned char ocd_cmd_optn_erase3[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_XDATA,    0x00, 0x80, 0x00 };
unsigned char ocd_cmd_optn_erase4[] = { CMD_Z8051_CMD, OCD_CMD_WRITE_CONT };
// F9 <- 0x04 don't clear any protections bits if active:
unsigned char ocd_cmd_optn_erase5[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0xa5, 0x02 };
// F9 <- 0x05 - force clear protection bits
unsigned char ocd_cmd_optn_erase5b[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0xa5, 0x02 };
unsigned char ocd_cmd_optn_erase6[] = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_SFR,      0xfe, 0x00, 0x00, 0x01 };
unsigned char ocd_cmd_optn_erase7[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x00 };
//
// flash read
unsigned char ocd_cmd_pgm_read1[]   = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_CODE,     0x00, 0x00, 0x00, PAGE_SIZE };
unsigned char ocd_cmd_pgm_read2[]   = { CMD_Z8051_CMD, OCD_CMD_READ_CONT, PAGE_SIZE };
//
// read cpu id
unsigned char ocd_cmd_cpuid[]       = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_BDC,      0x00, 0x00, 0x00, 0x02 };
//
// read run/stop state
unsigned char ocd_cmd_getrunstop1[] = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_BDC,      0x03, 0x00, 0x00, 0x01 };
//
// echo test <-> Arduino
unsigned char ocd_cmd_echo[]        = { CMD_ECHO,      'e', 'c', 'h', 'o' };
//
// SFR read
unsigned char ocd_cmd_sfr_read1[]   = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_SFR,      SFR_START, 0x00, 0x00, PAGE_SIZE };
unsigned char ocd_cmd_sfr_read2[]   = { CMD_Z8051_CMD, OCD_CMD_READ_CONT, PAGE_SIZE };
//
// SFR write
unsigned char ocd_cmd_sfr_write1[]  = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,         0, 0x00, 0x00,    0 };
//
// IRAM read
unsigned char ocd_cmd_iram_read1[]  = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_IRAM,     0x00, 0x00, 0x00, PAGE_SIZE };
unsigned char ocd_cmd_iram_read2[]  = { CMD_Z8051_CMD, OCD_CMD_READ_CONT, PAGE_SIZE };
//
// IRAM write
unsigned char ocd_cmd_iram_write1[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_IRAM,        0, 0x00, 0x00,    0 };
//
// XDATA read
unsigned char ocd_cmd_xdata_read1[] = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_XDATA,    0x00, 0x00, 0x00, PAGE_SIZE };
unsigned char ocd_cmd_xdata_read2[] = { CMD_Z8051_CMD, OCD_CMD_READ_CONT, PAGE_SIZE };
//
// XDATA write
unsigned char ocd_cmd_xdata_write1[]= { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_XDATA,       0, 0x00, 0x00,    0 };
//
// read PC (program counter)
// Reverse engineered with logic analyzer by using the ABOV OCD debugger software
unsigned char ocd_cmd_pc_read1[]    = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_DBG,      0x02, 0x00, 0x00, 0x03 };
//
// read registers from Background Debugging Controller
unsigned char ocd_cmd_bdc_read1[]   = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_BDC,      0x00, 0x00, 0x00,  0x00 };  // this WRITE is needed for the following READ to return data.
unsigned char ocd_cmd_bdc_read2[]   = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_BDC,      0x00, 0x00, 0x00,  0x20 };
//
// read registers from Debugging Logic
unsigned char ocd_cmd_dbg_read1[]   = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_DBG,      0x00, 0x00, 0x00,  0x20 };
//
// read registers from Debugging Trigger
unsigned char ocd_cmd_trg_read1[]   = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_TRG,      0x00, 0x00, 0x00,  0x20 };
//
// read registers from TRACER
unsigned char ocd_cmd_trc_read1[]   = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_TRACER,   0x00, 0x00, 0x00,  0x20 };
//
// read OPTION page
unsigned char ocd_cmd_optn_read1[]  = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x00 };
unsigned char ocd_cmd_optn_read2[]  = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x01 };
unsigned char ocd_cmd_optn_read3[]  = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x04 };
unsigned char ocd_cmd_optn_read4[]  = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x01 };
unsigned char ocd_cmd_optn_read5[]  = { CMD_Z8051_CMD, OCD_CMD_READ, OCD_DATA_CODE,      0x00, 0x00, 0x00, PAGE_SIZE };
unsigned char ocd_cmd_optn_read6[]  = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x00 };
unsigned char ocd_cmd_optn_read7[]  = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x01 };
//
// write OPTION page
unsigned char ocd_cmd_optn_wpage1[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x00 };
unsigned char ocd_cmd_optn_wpage2[] = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x01 };
unsigned char ocd_cmd_optn_wpage3[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xfe, 0x00, 0x00, 0x01 };
unsigned char ocd_cmd_optn_wpage4[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_XDATA,    0x00, 0x80, 0x00 };
unsigned char ocd_cmd_optn_wpage5[] = { CMD_Z8051_CMD, OCD_CMD_WRITE_CONT };
unsigned char ocd_cmd_optn_wpage6[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x04 };  // map OPTIONS page
unsigned char ocd_cmd_optn_wpage7[] = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x01 };
unsigned char ocd_cmd_optn_wpage8[] = { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xfa, 0x00, 0x00, 0x00, 0x00, 0x00, 0xa5, 0x03 };
unsigned char ocd_cmd_optn_wpage9[] = { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_SFR,      0xfe, 0x00, 0x00, 0x01 };
unsigned char ocd_cmd_optn_wpage10[]= { CMD_Z8051_CMD, OCD_CMD_WRITE, OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x00 };  // unmap OPTIONS page
unsigned char ocd_cmd_optn_wpage11[]= { CMD_Z8051_CMD, OCD_CMD_READ,  OCD_DATA_SFR,      0xf9, 0x00, 0x00, 0x01 };
