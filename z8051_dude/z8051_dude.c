/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <termios.h>
#include <fcntl.h>
#include <string.h>
#include <getopt.h>

#include "ocd_io.h"
#include "ocd_cmd.h"
#include "utils.h"

//***
//*** global variables
//***


//***
//*** prototypes
//***
void help(void);
void short_help(void);

//&&& return stati
//    0 - operation ok
#define STS_OK              0
#define STS_SETUP_FAIL      1
#define STS_ARG_FAIL        2
#define STS_CMD_FAIL        3
#define STS_CMD_NEEDCONFIRM 10

//***
//*** code
//***
int main(int argc, char **argv) {
   // locally used vars
   int sts=STS_OK;
   int ret;
   int address, value;

   // option flags
   static int iamsure = 0;
   static int bootloader=BOOTLOADER_OPTIBOOT;

   // optarg/getopt stuff
   int c;
   extern char *optarg;         /* Defined in libc getopt and unistd.h */
   int option_index = 0;
   static char short_options[] = "bcCd:eghijr:sSv:w:x";
#define OPT_SFR_READ        0x81
#define OPT_SFR_WRITE       0x82
#define OPT_IRAM_READ       0x83
#define OPT_XDATA_READ      0x84
#define OPT_REG_READ        0x85
#define OPT_OCD_READ        0x86
#define OPT_OPTION_READ     0x87
#define OPT_IRAM_WRITE      0x88
#define OPT_XDATA_WRITE     0x89
#define OPT_OPTION_WRITE    0x90
#define OPT_RESET           0x91
   static struct option long_options[] = {
      // position independend options
      {"ttyport",      required_argument, NULL,         'd'},
      {"help",         no_argument,       NULL,         'h'},
      {"intreset",     no_argument,       NULL,         'i'},
      {"sense",        no_argument,       NULL,         'j'},
      {"bl-optiboot",  no_argument,       &bootloader,  BOOTLOADER_OPTIBOOT},
      {"bl-arduino",   no_argument,       &bootloader,  BOOTLOADER_ATMEGABOOT},
      {"i-am-sure",    no_argument,       &iamsure,  1},
      // position dependend options (operations)
      // these will be executed in the order given
      {"cpuid",        no_argument,       NULL,         'c'},
      {"test",         no_argument,       NULL,         'e'},
      {"reset",        no_argument,       NULL,         OPT_RESET},
      {"go",           no_argument,       NULL,         'g'},
      {"continue",     no_argument,       NULL,         'C'},
      {"stop",         no_argument,       NULL,         's'},
      {"step",         no_argument,       NULL,         'S'},
      {"read",         required_argument, NULL,         'r'},
      {"write",        required_argument, NULL,         'w'},
      {"verify",       required_argument, NULL,         'v'},
      {"erase",        no_argument,       NULL,         'x'},
      {"blank",        no_argument,       NULL,         'b'},
      {"read-sfr",     no_argument,       NULL,         OPT_SFR_READ},
      {"write-sfr",    required_argument, NULL,         OPT_SFR_WRITE},
      {"read-iram",    no_argument,       NULL,         OPT_IRAM_READ},
      {"read-xdata",   no_argument,       NULL,         OPT_XDATA_READ},
      {"read-reg",     no_argument,       NULL,         OPT_REG_READ},
      {"read-ocd",     no_argument,       NULL,         OPT_OCD_READ},
      {"read-option",  no_argument,       NULL,         OPT_OPTION_READ},
      {"write-iram",   required_argument, NULL,         OPT_IRAM_WRITE},
      {"write-xdata",  required_argument, NULL,         OPT_XDATA_WRITE},
      {"write-option", required_argument, NULL,         OPT_OPTION_WRITE},
      {0,0,0,0}
   };


   char *filename = NULL;

   char *devname = NULL;
   int optionbytes = 0x0000;
   int fd;
   unsigned char reset_type=0;

   char *defdevname="/dev/ttyUSB0";
   devname = defdevname;

   if (argc == 1) {
      help();
      return(1);
   }


   // 1st option loop:
   // process options that are independent of the order they
   // are given, like serial device name, reset option, ...
   option_index = 0;
   while ((c = getopt_long(argc, argv, short_options,
                  long_options, &option_index)) != -1) {
      switch (c) {
         case 'i':
            reset_type = CMD_RESET_PWR_ARDUINO;
            break;
         case 'j':
            reset_type = CMD_RESET_PWR_EXTERN;
            break;
         case 'd':
            if ( optarg ) devname = optarg;
            break;
         case 'h':
            help();
            return(STS_OK);
         case '?':
            short_help();
            return(STS_OK);
     }
   }


   // now all information for establishing the communication is present.
   // connect to the target.
   fd = open(devname, O_RDWR | O_NOCTTY | O_SYNC);
   if (fd < 0) {
      printf("Error opening %s: %s\n", devname, strerror(errno));
      return(STS_SETUP_FAIL);
   }
   ret = set_ar_attr(fd, B115200);
   if (ret != 0) {
      printf("Error setting up serial interface\n");
      return(STS_SETUP_FAIL);
   }

   // perform a shortcut of the delay introduced by the optiboot
   // bootloader.
   bl_hack_fast_startup(fd, bootloader);

   // if requested, initialize device (init sequence with DCLK/DSDA toggles) 
   // to DBG mode goes first as other actions need this state
   if (reset_type > 0 ) {
      printf("waiting for target to power up...\n");
      ret=cmd_z8051_reset(fd, reset_type);
      if (ret < 0) { sts = STS_CMD_FAIL; }
   }


   // 2nd option run:
   // process position dependent options like
   // read/write/start/stop operations
   optind=1;
   option_index = 0;
   while ((c = getopt_long(argc, argv, short_options,
                  long_options, &option_index)) != -1) {
      switch (c) {
         // echo test
         case 'e':
            printf("send echo\n");
            ret=cmd_echo(fd);
            if ( ret >= 0) {
               printf("echo ok\n");
            } else {
               printf("echo NOK\n");
               sts =  STS_CMD_FAIL;
            } 
            break;
         // reset CPU (OCD reset)
         case OPT_RESET:
            ret=cmd_z8051_ocd_device_reset(fd);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // read CPU ID
         case 'c':
            ret=cmd_z8051_get_cpuid(fd, 1);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // execution - go
         case 'g':
            ret=cmd_z8051_runpgm(fd);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // execution - continue
         case 'C':
            ret=cmd_z8051_contpgm(fd);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // execution - stop
         case 's':
            ret=cmd_z8051_stoppgm(fd);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // execution - single step
         case 'S':
            ret=cmd_z8051_steppgm(fd);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // flash memory - read
         case 'r':
            if ( optarg ) filename = optarg;
            ret=cmd_z8051_read_pgm(fd, filename);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // flash memory - write
         case 'w':
            if ( optarg ) filename = optarg;
            ret=cmd_z8051_write_pgm(fd, filename);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // flash memory - verify
         case 'v':
            if ( optarg ) filename = optarg;
            ret=cmd_z8051_verify_pgm(fd, filename);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // flash memory - bulk erase
         case 'x':
            ret=cmd_z8051_bulk_erase(fd);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // flash memory - blank check
         case 'b':
            ret=cmd_z8051_blankcheck(fd);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // SFR - read
         case OPT_SFR_READ:
            ret=cmd_z8051_readsfr(fd);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // IRAM - read
         case OPT_IRAM_READ:
            ret=cmd_z8051_readiram(fd);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // XDATA - read
         case OPT_XDATA_READ:
            ret=cmd_z8051_readxdata(fd);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // REG - read
         case OPT_REG_READ:
            ret=cmd_z8051_readreg(fd);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // OCD - read
         case OPT_OCD_READ:
            ret=cmd_z8051_readocd(fd);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // OPTION - read
         case OPT_OPTION_READ:
            ret=cmd_z8051_option_page_read(fd, NULL);
            if (ret < 0) { sts = STS_CMD_FAIL; }
            break;
         // SFR - write
         case OPT_SFR_WRITE:
            if (optarg && (parse_addr_value_arg(optarg, &address, &value) == 0)) {
               ret=cmd_z8051_sfr_byte_write(fd, address, value);
               if (ret < 0) { sts = STS_CMD_FAIL; }
            } else {
               sts=STS_ARG_FAIL;
            }
            break;
         // IRAM - write
         case OPT_IRAM_WRITE:
            if (optarg && (parse_addr_value_arg(optarg, &address, &value) == 0)) {
               ret=cmd_z8051_iram_byte_write(fd, address, value);
               if (ret < 0) { sts = STS_CMD_FAIL; }
            } else {
               ret = STS_ARG_FAIL;
            }
            break;
         // XDATA - write
         case OPT_XDATA_WRITE:
            if (optarg && (parse_addr_value_arg(optarg, &address, &value) == 0)) {
               ret=cmd_z8051_xdata_byte_write(fd, address, value);
               if (ret < 0) { sts = STS_CMD_FAIL; }
            } else {
               sts=STS_ARG_FAIL;
            }
            break;
         // OPTION - write
         case OPT_OPTION_WRITE:
            if ( optarg ) optionbytes = strtoul(optarg, 0, 16);
            if (iamsure == 1) {
               ret=cmd_z8051_option_byte_write(fd, optionbytes);
               if (ret < 0) { sts = STS_CMD_FAIL; }
            } else {
               printf("Possibly destructive operation, confirm with --i-am-sure\n");
               sts = STS_CMD_NEEDCONFIRM;
            }
            break;
     } // switch
   } // while

   return sts;
}


void short_help(void) {
   fprintf (stderr, "Usage: z8051_dude OPTION... [FILE]... .\n");
}


void help(void) {
   fprintf (stderr, "Usage: z8051_dude OPTION... [FILE]... .\n");
   fprintf (stderr, "\n");
   fprintf (stderr, "   -h, -?, --help    this help\n");
   fprintf (stderr, "   -e, --test        test communiaction with arduino board\n");
   fprintf (stderr, "   --ttyport </dev/ttyxx>\n");
   fprintf (stderr, "   -d </dev/ttyxx>   tty of arduino board. Default device is /dev/ttyUSB0\n");
   fprintf (stderr, "   -i, --intreset    reset of MCU is driven power from arduino\n");
   fprintf (stderr, "   -j, --sense       reset of MCU is driven from extern. Power is sensed\n");
   fprintf (stderr, "                     by arduino board\n");
   fprintf (stderr, "   -c, --cpuid       get cpu id of MCU\n");
   fprintf (stderr, "   --bl-arduino      assume standard Arduino ATmegaBOOT loader\n");
   fprintf (stderr, "   --bl-optiboot     assume Optiboot loader (default)\n");
   fprintf (stderr, "Memory manipulation:\n");
   fprintf (stderr, "   --read filename\n");
   fprintf (stderr, "   -r filename       read program memory and write to file\n");
   fprintf (stderr, "   --write filename\n");
   fprintf (stderr, "   -w filename       read file and program to memory\n");
   fprintf (stderr, "   --verify filename\n");
   fprintf (stderr, "   -v filename       verify program memory against file\n");
   fprintf (stderr, "   -x, --erase       erase flash memory\n");
   fprintf (stderr, "   -b, --blank       blank check\n");
   fprintf (stderr, "   --read-sfr        read SFR memory and display\n");
   fprintf (stderr, "   --write-sfr <addr>=<value> (e.g. 0xc8=0x20)\n");
   fprintf (stderr, "                     write a value into SFR register\n");
   fprintf (stderr, "   --read-iram       read IRAM memory and display\n");
   fprintf (stderr, "   --read-xdata      read XDATA memory and display\n");
   fprintf (stderr, "   --read-reg        read CPU registers\n");
   fprintf (stderr, "   --read-ocd        read OCD BDC/DBG/TRG registers\n");
   fprintf (stderr, "   --read-option     read OPTION page\n");
   fprintf (stderr, "   --write-option <hexvalue> (e.g. 0x0000)\n");
   fprintf (stderr, "                     write OPTION bytes 0x3e (LSB) and 0x3f (MSB)\n");
   fprintf (stderr, "Execution control:\n");
   fprintf (stderr, "   --reset           reset the MCU state (OCD reset)\n");
   fprintf (stderr, "   -g, --go          go - run program\n");
   fprintf (stderr, "   -s, --stop        stop running program\n");
   fprintf (stderr, "   -C, --continue    continue program execution\n");
   fprintf (stderr, "   -S, --step        single step program execution\n");
}
