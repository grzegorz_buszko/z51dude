/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


//***
//*** macros
//***
#ifndef MIN
    #define MIN(a,b)    (a < b)? a:b
#endif


//***
//*** constants
//***
#define     IMAGE_BUFSIZE   65536
#define     IHEX_RECSIZE    0x10
#define     IHEX_SKIP_00    1       // optimize away 0x00 blocks in writing


//***
//*** prototypes
//***
int ihex_load (char* filename);
int ihex_fetch(unsigned char *buffer, int size);
int ihex_write(unsigned char *buffer, int size, unsigned int addr, FILE *fp);
int ihex_write_eof(FILE *fp);
