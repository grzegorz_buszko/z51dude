/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

//***
//*** macros
//***
#ifndef MAX
    #define MAX(a,b)    (a > b)? a:b
#endif


//***
//*** constants
//***
#define OCD_CMD_WRITE_CONT    0x10
#define OCD_CMD_WRITE         0x12
#define OCD_CMD_READ_CONT     0x20
#define OCD_CMD_READ          0x22

#define OCD_DATA_CODE         0x10  // Z51F3220, Code Area
#define OCD_DATA_XDATA        0x11  // Z51F3220, OCD Data Area in the range 0x8000–0x803F
#define OCD_DATA_IRAM         0x12  // MC96F6432, IRAM Area
#define OCD_DATA_SFR          0x13  // Z51F3220, Special Function Registers
#define OCD_DATA_BDC          0x20  // Z51F3220, Background Debugging Controller
#define OCD_DATA_DBG          0x21  // Z51F3220, Debugging Logic
#define OCD_DATA_TRG          0x22  // Z51F3220, Debugging Trigger
#define OCD_DATA_TRACER       0x23  // OCD Tracer (seen in the wild)

#define PAGE_SIZE             0x40  // page size for memory access (flash read/write)

// SFR memory of MCS51 core: 0x00 - 0xFF (is global and *not* device specific)
#define SFR_START    0x80
#define SFR_SIZE     0x80

// size of read & write buffer towards arduino
#define MAXBUF                0x4f


//***
//*** prototypes
//***
int cmd_echo(int );
int cmd_z8051_reset(int , unsigned char );
int cmd_z8051_get_cpuid(int, int);
int cmd_z8051_ocd_get_runstop(int);
int cmd_z8051_ocd_device_reset(int);

int cmd_z8051_read_pgm(int, char*);
int cmd_z8051_write_pgm(int, char*);
int cmd_z8051_verify_pgm(int, char*);
int cmd_z8051_blankcheck(int);

int cmd_z8051_bulk_erase(int);
int cmd_z8051_option_page_read(int fd, unsigned char *pagebuff);
int cmd_z8051_option_byte_write(int fd, unsigned int optionbytes);


int cmd_z8051_runpgm(int);
int cmd_z8051_contpgm(int);
int cmd_z8051_stoppgm(int);
int cmd_z8051_steppgm(int);

int cmd_z8051_readsfr(int);
int cmd_z8051_sfr_byte_write(int fd, int address, int value);
int cmd_z8051_readiram(int fd);
int cmd_z8051_iram_byte_write(int fd, int address, int value);
int cmd_z8051_readxdata(int fd);
int cmd_z8051_xdata_byte_write(int fd, int address, int value);
int cmd_z8051_readreg(int fd);
int cmd_z8051_readocd(int fd);

