/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "intelhex.h"


//***
//*** global variables
//***
static uint8_t  buffer_initialized=0;
static int      offset_top=0;
static int      offset_current=0;
static uint8_t  imagebuffer[IMAGE_BUFSIZE];


//***
//*** code
//***

/*
 * load I8HEX file into memory and return last offset,
 * or <0 if error.
 * 
 * :1009B70040E87A107B007D40E4FFFE1208B89010F3
 * sllaaaattddddddddddddddddddddddddddddddddcc
 * 01234567890
 * s - start indicator
 * l - length
 * a - address
 * t - record type
 * d - data
 * c - checksum
 * 
 */
int ihex_load (char* filename) {
    FILE *fp;
    int offset;
    int maxoffset=0;

    if (filename == NULL) {
        fprintf(stderr, "NULL filename passed to ihex_load()\n");
        return(-1);
    }

    fp = fopen(filename,"r");
    if(fp == NULL) {
        perror("Error opening file");
        return(-1);
    }

    // initialize target image memory
    memset(imagebuffer, 0, sizeof(imagebuffer));
    memset(imagebuffer, 0, IMAGE_BUFSIZE);

    for (;;) {
        char line[128];
        char *c;
        char tmp[8];
        int len;
        int type;
        int i;
        int checksum1;
        int checksum2;

        // read one line from file
        c = fgets(line, sizeof(line)-1, fp);
        if (c == NULL) {
            printf("encountered unexpected enf-of-file before EOF record\n");
            return(-1);
        }

        line[sizeof(line)-1]='\0';

        // skip comments
        if (line[0] == '#')  continue;

        // remove newlines
        c = strchr (line, '\n');
        if (c) *c = 0;

        // skip comments
        if (line[0] != ':') {
            fprintf(stderr, "this does not look like an IntelHEX file to me\n");
            return(-2);
        }

        // get length
        memcpy(tmp, &line[1], 2);
        tmp[2]='\0';
        len=strtoul(tmp, 0, 16);
        // check for max length
        if (len > 32) {
            fprintf(stderr, "IntexHEX record length is too big at address 0x%04x\n", offset);
            return(-3);
        }

        // get address offset
        memcpy(tmp, &line[3], 4);
        tmp[4]='\0';
        offset=strtoul(tmp, 0, 16);

        // if offset > imagebuffer -> fail
        if (offset > IMAGE_BUFSIZE) {
            fprintf(stderr, "IntexHEX address offset outside allowed range\n");
            return(-4);
        }

        // get record type
        memcpy(tmp, &line[7], 2);
        tmp[2]='\0';
        type=strtoul(tmp, 0, 16);

        // EOF record (type=1)
        if (type == 1) break;

        // remaining types other than 0? unsupported!
        if (type != 0) {
            fprintf(stderr, "unsupported IntelHEX record type at address 0x%04x\n", offset);
            return(-5);
        }

        // consistency check for length indicator and buffer length
        // n characters in line where n=
        // 1 char startindicator
        // 2 char length
        // 4 char address
        // 2 char type
        // 2*len char data
        // 2 char checksum 
        if ( (len*2) + 11 > strlen(line)) {
            fprintf(stderr, "IntelHEX record is too short at address 0x%04x\n", offset);
            return(-6);
        }

        // fetch checksum byte
        memcpy(tmp, &line[9+(2*len)], 2);
        tmp[2]='\0';
        checksum1=strtoul(tmp, 0, 16);
        // calculate checksum over the line
        checksum2=0;
        for (i=0; i < 4+len; i++) {
            memcpy(tmp, &line[(2*i)+1], 2);
            tmp[2]='\0';
            checksum2+=strtoul(tmp, 0, 16);
        }
        checksum2=(-1*checksum2) & 0xFF;

        // check for checksum error
        if (checksum1 != checksum2) {
            fprintf(stderr, "InetlHEX checksum error at offset 0x%04x\n", offset);
            return (-7);
        }

        // loop through data, store to image buffer and verify checksum
        for (i=0; i < len; i++) {
            memcpy(tmp, &line[9+(2*i)], 2);
            tmp[2]='\0';
            imagebuffer[offset++]=strtoul(tmp, 0, 16);
        }

        // remember max write offset
        if (offset > maxoffset) {
            maxoffset = offset;
        }

    } // for (;;)

    buffer_initialized=1;
    offset_top=maxoffset-1;
    offset_current=0;

    return(offset_top);
}


/*
 * read data from image buffer
 *
 * returns number of read bytes
 */
int ihex_fetch(unsigned char *buffer, int size) {
    int i;

    if (buffer_initialized == 0) {
        fprintf(stderr, "no IntelHEX file loaded\n");
        return(-1);
    }

    if (buffer==0) {
        fprintf(stderr, "NULL pointer passed to ihex_fetch()\n");
        return(-1);
    }

    memset(buffer, 0, size);
    for (i=0; i < size; i++) {
        // offset_current points to nex to be read byte in imagebuffer
        // offset_top points to the last written byte in imagebuffer
        if (offset_current > offset_top) break;
        buffer[i]=imagebuffer[offset_current++];
    }

    return(i);
}


/*
 * write IntelHEX records with data in buffer
 */
int ihex_write(unsigned char *buffer, int size, unsigned int addr, FILE *fp) {
    int i, j;
    int len;
    int checksum=0;
    
#if IHEX_SKIP_00
    for (i=0; i < size; i++) {
        if (buffer[i] != 0x00) break;
    }
    if (i == size) return 0;
#endif

    for (i=0; i< size;) {
        len = MIN(size-i, IHEX_RECSIZE);
        checksum=len + ((addr >> 8) & 0xff) + (addr & 0xff);
        fprintf(fp, ":%02X%04X00", len, addr);
        for (j=0; j< size && j < IHEX_RECSIZE; j++) {
            // write records of up to IHEX_RECSIZE bytes of data
            checksum += buffer[i+j];
            fprintf(fp, "%02X", buffer[i+j]);
        }
        fprintf(fp, "%02X\n", (-1*checksum) & 0xff);
        addr+=j;
        i+=j;
    }
    return i;
}


/*
 * write IntelHEX EOF record
 */
int ihex_write_eof(FILE *fp) {
    fprintf(fp, ":00000001FF\n");
    return 0;
}
