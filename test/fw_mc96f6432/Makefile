#
#    z51dude
#    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

# REQUIREMENTS:
# To build this, you need the sdcc (small device C compiler) package installed

OBJLIB_test_led =
OBJLIB += $(OBJLIB_test_led)

# list of binaries
HEXFILES=test_led.hex

# sdcc specifics
CC=sdcc-sdcc
MCU=-mmcs51
INC=-I.


# optimize for size:
# MC96F6432: IRAM=256 XRAM=768 CODE=32k
CFLAGS=$(MCU) $(INC) --iram-size 256 --xram-size 768 --xram-loc 0 --code-size 32768  --model-large

# magic (http://www.bgoncalves.com/notes/2007/03/20/test/)
.SUFFIXES:
.PHONY :
.PRECIOUS : %.rel %.lst

# the name of the related file that caused the action.
INFILE = $<
# is the name of the file to be made.
OUTFILE = $@
# the prefix shared by target and dependent files.
OUTBASE = $*

all: half-clean $(HEXFILES)
	@echo "all done"

%.rel: %.c
	$(CC) $(CFLAGS) -o $(OUTFILE) -c $(INFILE)

%.hex : %.rel $(OBJLIB) 
	$(CC) $(CFLAGS) -o $(OUTFILE) $(INFILE) $(OBJLIB_$(OUTBASE))

%.bin: %.hex
	srec_cat  -Output $(OUTFILE) -Binary  $(INFILE) -Intel


# erase the device before it is programmed
%.load: %.hex
	# check assumed device with signature bytes
	# z51dude ...

clean:
	@echo "Cleaning up..."
	@rm -f *.rel *.asm *.lst *.sym *.lk *.mem *.map *.rst *.hex *.adb *.cdb *.omf *.bin

half-clean:
	@echo "Cleaning up a bit..."
	@rm -f *.hex
